// https://alistapart.com/article/fixing-variable-scope-issues-with-ecmascript-6/

// using let - ES6
// function doSomething() {
//   let myVar = 1;
//   if (true) {
//       // using let keyword gives us block scope
//     let myVar = 2;
//     console.log(2);
//   }
//   console.log(1);
// }

// doSomething();

function doSomething() {
  let myVar = 1;
  if (true) {
    console.log(myVar);
    let yourVar1 = 2;
    console.log(yourVar1);
  }
}

doSomething();
